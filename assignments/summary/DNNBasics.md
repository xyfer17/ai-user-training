# Deep Neural Network(DNN)

* A DNN is a network with multiple layers of interconnected neurons between the input and ouput layers. These in between layers are known as hidden layers.

![DNN](/assignments/summary/DNN_image/DNN.png)

## Neuron
* Thing that holds a number ranging from 0 to 1 known as its activation . 
* Neuron is lit up, when its activation high number or nearly to 1 and dim down,when its activation least number or nearly to 0.

![neuron](/assignments/summary/DNN_image/neuron.png)
 
## What is layers ?
* Layers of the Neural Network takes input from previous layers and helps in calculating the probability of the output.
* In the neural network there are 3 types of layers
1. Input layer — initial data for the neural network.
2. Hidden layers — intermediate layer between input and output layer and place where all the computation is done.
3. Output layer — produce the result for given inputs.

<div align="center">![Layers](/assignments/summary/DNN_image/layers.jpg)</div>

## Weights and bias
* **Weights** are the co-efficients of the equation which you are trying to resolve. Negative weights reduce the value of an output.
* **Bias** is simply a constant value (or a constant vector) that is added to the product of inputs and weights. Bias is utilised to offset the result.

<div align="center"> ![weight_bias](/assignments/summary/DNN_image/weight and bias.png) </div>


### What parameters should exists in a DNN ?
* It consist the weight for each activation of the input neurons connected to neurons in next layer and a bias which is added to weighted sum. This is done for making the network able to find the desired pattern.

![parameter](/assignments/summary/DNN_image/parameter.png)

## Sigmoid Function
* This function always return a value between 0 and 1, no matter what the input is.
* Graph showing behavior of sigmoid function.

![sigmoid](/assignments/summary/DNN_image/Sigmoid_Function_Graph.png)

## ReLU Function
* It stands for Rectified Linear Unit which gives 0 for negative and acts as identity function for positive values.
Results in faster DNNs.

![ReLU](/assignments/summary/DNN_image/ReLU_Graph.png)

### Cost Function
* It is used to determine how bad is the network. Needs to be minimised for a better DNN.

![Cost_function](/assignments/summary/DNN_image/cost_function.png)

## Gradient Descent
* Gradient descent is a first-order iterative optimization algorithm for finding a local minimum of a differentiable function.
* For the minimization of the output we use the gradient descent algorithm.It finds the local minimum of a function.
![Gradient_descent](/assignments/summary/DNN_image/zHvNe.png)

### Backpropagation
* It is an algorithm which calculates the gradient for the gradient descent algorithm.This is done by starting from the output layer and reverse engineering the desired changes.
* Proper tuning of the weights allows you to reduce error rates and to make the model reliable by increasing its generalization.

<div align="center">![Backprog](/assignments/summary/DNN_image/1_LB10KFg5J7yK1MLxTXcLdQ.jpeg)</div>

### Stochastic Gradient Descent
* It's a technique in which the large training data is divided into mini batches and gradient
descent of those mini batches is found using backprop iteratively. 
* It is computationally faster than finding the gradient of whole training data at a time.

![Stochastic](/assignments/summary/DNN_image/Stochastic Gradient Descent.png)

> THIS IS ALL FROM MY SIDE ,FOR MORE INFORMATION GO THROUGH THE [WIKIPEDIA](https://en.wikipedia.org/wiki/Deep_learning) AND ABOVE YOUTUBE CHANNEL [3BLUE1BROWN](https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be) .
> <div>:+1: :blush: :star: :wave:</div>








